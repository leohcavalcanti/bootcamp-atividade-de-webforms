﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaDaSemana.aspx.cs" Inherits="Calculadora.DiaDaSemana" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Disponibilidade de trabalho</title>
</head>
<body>
    <h1>Disponibilidade de trabalho</h1>
    <form id="disponibilidade" runat="server">
        <asp:CheckBoxList ID="diaCBL" RepeatDirection="Horizontal" runat="server">
            <asp:ListItem Text="Segunda-Feira" value="segunda-feira"></asp:ListItem>
            <asp:ListItem Text="Terça-Feira" value="terça-feira"></asp:ListItem>
            <asp:ListItem Text="Quarta-Feira" value="quarta-feira"></asp:ListItem>
            <asp:ListItem Text="Quinta-Feira" value="quinta-feira"></asp:ListItem>
            <asp:ListItem Text="Sexta-Feira" value="sexta-feira"></asp:ListItem>
            <asp:ListItem Text="Sábado" value="sábado"></asp:ListItem>
            <asp:ListItem Text="Domingo" value="domingo"></asp:ListItem>
        </asp:CheckBoxList>
        <asp:Button ID="Button1" runat="server" Text="Enviar" OnClick="Button1_Click" />
    </form>
    <br />
    <asp:Label ID="Result" runat="server" Text="Dias Selecionados"></asp:Label>
</body>
</html>
