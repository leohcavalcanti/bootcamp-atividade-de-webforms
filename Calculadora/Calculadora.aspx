﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="Calculadora.Calculadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CALCULADORA</title>
</head>
<body>
    <h1>Calculadora</h1>
    <form id="calculadora" runat="server">
        <asp:Label Text="Número 1" runat="server"></asp:Label>
        <asp:TextBox ID="numero1" ReadOnly="false" TextMode="Number" runat="server"></asp:TextBox>

        
        <p>
            <asp:Label Text="Número 2" runat="server"></asp:Label>
            <asp:TextBox ID="numero2" ReadOnly="false" TextMode="Number" runat="server"></asp:TextBox>
        </p>
        <asp:Label Text="Operação" runat="server"></asp:Label>
        <asp:DropDownList ID="operacao" runat="server">
            <asp:ListItem Value="Adicao" Text="Adição"></asp:ListItem>
            <asp:ListItem Value="Subtracao" Text="Subtração"></asp:ListItem>
            <asp:ListItem Value="Multiplicacao" Text="Multiplicação"></asp:ListItem>
            <asp:ListItem Value="Divisao" Text="Divisão"></asp:ListItem>
        </asp:DropDownList>

        <br />
        <br />

        <asp:Button ID="Button1" runat="server" Text="Calcular" BackColor="#0099CC" BorderStyle="Ridge" OnClick="Button1_Click" />
        
        <br />

        <asp:Label ID="result" Text="Resultado" runat="server"></asp:Label>
    </form>
</body>

</html>
