﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculadora
{
    public partial class Calculadora : System.Web.UI.Page
    {
        protected void Button1_Click(object sender, EventArgs e)
        {
            String operation = operacao.SelectedValue;

            int numeroUm = Convert.ToInt32(numero1.Text);
            int numeroDois = Convert.ToInt32(numero2.Text);

            switch (operation)
            {
                case "Adicao":
                    result.Text = "Resultado = " + (numeroUm + numeroDois);
                    break;
                case "Subtracao":
                    result.Text = "Resultado = " + (numeroUm - numeroDois);
                    break;
                case "Multiplicacao":
                    result.Text = "Resultado = " + (numeroUm * numeroDois);
                    break;
                case "Divisao":
                    result.Text = "Resultado = " + (numeroUm / numeroDois);
                    break;
            }
        }
    }
}